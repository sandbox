<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Apple</title>
</head>
<body>
<?php
$value1 = 10;

echo "Value 1 (1): " . $value1;
echo "<br />";

echo "-- Call &ref_value1(&$value1)";
$value2 = &ref_value1($value1);
echo "<br />";

echo "Value 1 (2) : " . $value1;
echo "<br />";

echo "Value 2 (1): " . $value2;
echo "<br />";

echo "Type of Value 2 : " . gettype($value2);
echo "<br />";

echo "-- Change value1 = '100'";
echo "<br />";
$value1 = 100;

echo "Value 1 (3) : " . $value1;
echo "<br />";


echo "Value 2 (2): " . $value2;
echo "<br />";




//Function Return Reference of $value1
function &ref_value1(&$value)
{
    $value += 10;
    return $value;
}
?>
</body>
</html>