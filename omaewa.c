#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void omaewa(int signo) {
    puts("Omae wa mou shindeiru");
    exit(1);
}

int main() {
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &omaewa;
    sigaction(SIGSEGV, &sa, NULL);

    char *ro = "Hello world!";
    ro[0] = '\0'; /* oops */

    return 0;
}
