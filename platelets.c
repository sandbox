// clang $(sdl2-config --cflags) $(sdl2-config --libs) -lSDL2_mixer platelets.c
// https://platelets.unhexium.dev/public/scripts/counter.js
// https://platelets.unhexium.dev/public/sounds/{music,poke,react{1,2}}.ogg

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "SDL_mixer.h"

Mix_Music *music = NULL;
Mix_Chunk *poke = NULL;
Mix_Chunk *react[2] = {NULL, NULL};

void cleanup() {
    if (music != NULL) Mix_FreeMusic(music);
    if (poke != NULL) Mix_FreeChunk(poke);
    if (react[0] != NULL) Mix_FreeChunk(react[0]);
    if (react[1] != NULL) Mix_FreeChunk(react[1]);
    Mix_CloseAudio();
}

int main() {
    char buf[16];
    unsigned int clicks = 0;
    srand(time(NULL));
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);
    Mix_AllocateChannels(2);
    music = Mix_LoadMUS("music.ogg");
    poke = Mix_LoadWAV("poke.ogg");
    react[0] = Mix_LoadWAV("react1.ogg");
    react[1] = Mix_LoadWAV("react2.ogg");
    Mix_PlayMusic(music, -1);
    while (1) {
        printf("Press enter");
        if (fgets(buf, 2, stdin)) {
            printf("Poke!\n");
            Mix_PlayChannel(0, poke, 0);
            if (++clicks % 6 == 0) {
                int reaction = rand() & 1;
                printf("Reaction %d\n", reaction);
                Mix_PlayChannel(1, react[reaction], 0);
            }
        }
    }
    atexit(cleanup);
}
